/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.ctu.fit.ddw;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author Pavel
 */
public class Youtube {

    public String Search(String query) {
        StringBuilder sb = new StringBuilder();

        Document doc;
        try {
            doc = Jsoup.connect("https://www.youtube.com/results?search_query=" + query).get();
            Elements els = doc.getElementsByClass("item-section").select("h3").select("a[href]");
            ArrayList<String> links = new ArrayList<String>();
            for (int i = 0; i < els.size(); i++) {
//                links.add("http://youtube.com" + els.get(i).attr("href"));
                String href = els.get(i).attr("href");
                links.add("https://www.youtube.com/all_comments?v=" + href.substring(href.lastIndexOf("=") + 1));
            }


            ArrayList<String> comments = new ArrayList<String>();


            for (int i = 0; i < links.size(); i++) {
//                https://www.youtube.com/all_comments?v=zaYFLb8WMGM
                try {
                    System.out.println("Downloading " + links.get(i));
                    doc = Jsoup.connect(links.get(i)).get();
                    els = doc.getElementsByClass("comment-text-content");
                    for (int j = 0; j < els.size(); j++) {
                        String text = els.get(j).text();
                        comments.add(text);
                        sb.append(text + " ");
                    }
                } catch (Exception e) {
                }

            }

            System.out.println("hi");

        } catch (IOException ex) {
            Logger.getLogger(Youtube.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(sb.toString());
        SaveToDisk(sb.toString());
        return sb.toString();
    }

    //http://stackoverflow.com/questions/1459656/how-to-get-the-current-time-in-yyyy-mm-dd-hhmisec-millisecond-format-in-java
    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private void SaveToDisk(String text) {
        String fileName = getCurrentTimeStamp() + ".txt";
        File file = new File(fileName);
        try {
            System.out.println("Going to save ...");
               
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(text);
            bw.close();


//        System.out.println("Done");
            System.out.println("Saved to file " + fileName);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //http://stackoverflow.com/questions/238547/how-do-you-programmatically-download-a-webpage-in-java
    private String downloadPageString(String website) {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;

        try {
            url = new URL(website);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return null;
    }
}
