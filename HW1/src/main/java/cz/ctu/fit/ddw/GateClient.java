
package cz.ctu.fit.ddw;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci
 * www: http://dojchinovski.mk
 */
public class GateClient {
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    private String query = "";

    public GateClient(String query) {
        this.query = query;
    }
    
    
    
    public void run(){
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        

        try {                
            
            
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            ProcessingResource gazeteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            
            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            
            // locate the JAPE grammar file
            File japeOrigFile = new File("c:\\Users\\Pavel\\Dropbox\\Skola\\FIT\\DDW\\jape-example.jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
//            annotationPipeline.add(tokenizerPR);
//            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(gazeteer);
            annotationPipeline.add(japeTransducerPR);
            
                  Youtube yt = new Youtube();
                    
            
            // create a document
            Document document = Factory.newDocument(yt.Search(query));
//            Document document2 = Factory.newDocument("Maggie Daley Park is a 20-acre (81,000 m2) public park in the Loop community area of Chicago. It is near the Lake Michigan shoreline in northeastern Grant Park where Daley Bicentennial Plaza previously stood.[1][2] Designed by landscape architect Michael Van Valkenburgh, the park had its ceremonial ribbon cutting on December 13, 2014, and is named for Maggie Daley, the former First Lady of the city who died in 2011.[3][4] This Grant Park section is bounded by Randolph Street, Monroe, Columbus and Lake Shore Drives.[5] The park, which cost $60 million, began construction two years before the 2014 opening.[6] It is connected to Millennium Park by the BP Pedestrian Bridge.[6]");
//            Document document3 = Factory.newDocument("The Center for Biofilm Engineering (CBE) is an interdisciplinary research, education, and technology transfer institution located on the central campus of Montana State University in Bozeman, Montana. The center was founded in April 1990 as the Center for Interfacial Microbial Process Engineering with a grant from the Engineering Research Centers (ERC) program of the National Science Foundation (NSF).[1] The CBE integrates faculty from multiple university departments to lead multidisciplinary research teams—including graduate and undergraduate students—to advance fundamental biofilm knowledge, develop beneficial uses for microbial biofilms, and find solutions to industrially relevant biofilm problems. The center tackles biofilm issues including chronic wounds, bioremediation, and microbial corrosion through cross-disciplinary research and education among engineers, microbiologists and industry.");

            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);
//            corpus.add(document2);
//            corpus.add(document3);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){

                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();

                FeatureMap futureMap = null;
                // get all Token annotations
                AnnotationSet annSetCountry = as_default.get("Country",futureMap);
                System.out.println("Number of Country annotations: " + annSetCountry.size());
                AnnotationSet annSetWomen = as_default.get("Woman",futureMap);
                System.out.println("Number of Woman annotations: " + annSetWomen.size());
                AnnotationSet annSetMan = as_default.get("Man",futureMap);
                System.out.println("Number of Man annotations: " + annSetMan.size());
                
                AnnotationSet annSetPos = as_default.get("Positive",futureMap);
                System.out.println("Number of Positive annotations: " + annSetPos.size());
                AnnotationSet annSetNeg = as_default.get("Negative",futureMap);
                System.out.println("Number of Nagative annotations: " + annSetMan.size());

                
                Map<String, Integer> hm = new HashMap<String, Integer>();
                ArrayList tokenAnnotations = new ArrayList(annSetCountry);
                
                
                // looop through the Token annotations
                for(int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation)tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    
                    
                    if (hm.containsKey(underlyingString))
                    {
                        Integer count = hm.get(underlyingString);
                        hm.remove(underlyingString);
                        hm.put(underlyingString, count + 1);
                    }
                    else
                    {
                        hm.put(underlyingString,  1);
                    }
                    
//                    System.out.println("Token: " + underlyingString);
//                    
//                    // get the features of the token
//                    FeatureMap annFM = token.getFeatures();
                    
                    // get the value of the "string" feature
                    //String value = (String)annFM.get((Object)"string");
                    //System.out.println("Token: " + value);
                }
                System.out.println(new PrettyPrintingMap<String, Integer>(hm));
                
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   //http://stackoverflow.com/questions/10120273/pretty-print-a-map-in-java
    public class PrettyPrintingMap<K, V> {
    private Map<K, V> map;

    public PrettyPrintingMap(Map<K, V> map) {
        this.map = map;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Entry<K, V>> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<K, V> entry = iter.next();
            sb.append(entry.getKey());
            sb.append('=').append('"');
            sb.append(entry.getValue());
            sb.append('"');
            if (iter.hasNext()) {
                sb.append(',').append(' ');
            }
        }
        return sb.toString();

    }
}

    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:\\Program Files\\GATE_Developer_8.0\\");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:\\Program Files\\GATE_Developer_8.0\\plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("C:\\Program Files\\GATE_Developer_8.0\\", "gate.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}